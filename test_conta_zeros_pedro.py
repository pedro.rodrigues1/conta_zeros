import unittest
import conta_zeros_pedro


class TestConta_zeros(unittest.TestCase):

    def test_retorna000(self):
        print("Teste - 503149203103000: ", end="")
        texto = "503149203103000"
        teste = conta_zeros_pedro.conta_zeros(texto)
        self.assertEqual(3, teste)

    def test_retorna0000(self):
        print("Teste - 110001010000111: ", end="")
        texto = "110001010000111"
        teste = conta_zeros_pedro.conta_zeros(texto)
        self.assertEqual(4, teste)

    def test_retorna_nenhum_zero(self):
        print("Teste - 112336496: ", end="")
        texto = "112336496"
        teste = conta_zeros_pedro.conta_zeros(texto)
        self.assertEqual(0, teste)

    def test_retorna_input_com_espacos(self):
        print("Teste - 6314 000 111 00000001: ", end="")
        texto = "6314 000 111 00000001"
        teste = conta_zeros_pedro.conta_zeros(texto)
        self.assertEqual(7, teste)


if __name__ == '__main__':
    unittest.main()
